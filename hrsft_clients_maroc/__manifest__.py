
{
    'name': 'Client (RC,IF,PATENTE,ICE)',
    'version': '13.0',
    'author': 'OUARRAQiSoufiane',
    'category': 'Management',
    'summary': """Fiche partenaire Maroc""",
    'license': 'AGPL-3',
    'website': 'www.versusarena.ma',
    'description': """Ajouts des champs RC(Registre de Commerce, IF (Identifiant Fiscale), ICE (Identifiant ommun de l'Entreprise)
dans la fiche client en conformité aux normes Maroc

    """,
    'depends': ["base"],
    'data': [
             'views/partner_view.xml',
             ],
    'images': ['static/description/banner.png'],
    'installable': True,
    'application' : True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
